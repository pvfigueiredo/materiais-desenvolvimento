# README #

Repositório criado para guardar materiais de interesse do setor de desenvolvimento

### Materiais Desenvolvimento ###

* Versão 1.0.0

### Materiais ###


### Contribution guidelines ###

* Faça o pull: $git pull origin master
* Crie uma nova branch a partir da master: $git checkout -b "nomebranch"
* Adicione as modificações feitas: $git add .
* Faça o commit: $git commit -m "Detalhe sobre as modificações adicionadas"
* Antes de fazer o merge na master, confirme que ela esteja atualizada: $git pull origin master
* Efetue o merge da brach na master: $git merge "nomebranch" master
* Envie as modificações: $git push origin master

### Owner ###

* Paulo Figueiredo